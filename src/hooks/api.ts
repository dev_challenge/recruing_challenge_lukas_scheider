import axios, { AxiosResponse } from "axios";

const API_KEY = "56edad45";
const BASE_URL = "https://www.omdbapi.com/";

interface Movie {
  Title: string;
  Year: string;
  Genre: string;
  Plot: string;
  Type: string;
  Released: string;
  Poster: string;
  imdbID: string;
}

interface SearchResponse {
  Search: Movie[];
  totalResults: string;
  Response: string;
}

export const searchMovies = async (searchTerm: string, type: string): Promise<SearchResponse> => {
  const response: AxiosResponse<SearchResponse> = await axios.get(`${BASE_URL}?s=${searchTerm}&type=${type}&apikey=${API_KEY}`);

  const moviesWithGenre = await Promise.all(response.data.Search.map(async movie => {
    const details = await getMovieDetails(movie.imdbID);
    return { ...movie, Genre: details.Genre };
  }));
  return { ...response.data, Search: moviesWithGenre };
};

export const getMovieDetails = async (movieId: string): Promise<Movie> => {
  const response: AxiosResponse<Movie> = await axios.get(`${BASE_URL}?i=${movieId}&apikey=${API_KEY}`);
  return response.data;
};