import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import delete_icon from '../assets/icons8-delete.gif';
import delete_icon_static from '../assets/delete_icon_static.gif';
import soundFile from '../assets/sound.mp3';
import cameraIcon from '../assets/video-camera.png';

interface Movie {
  Title: string;
  Year: string;
  Genre: string;
  Plot: string;
  Type: string;
  Released: string;
  Poster: string;
  imdbID: string;
}

const FavouritesPage = () => {
  const [favourites, setFavourites] = useState<Movie[]>([]);
  const [isHovered, setIsHovered] = useState<string | null>(null);

  useEffect(() => {
    const loadedFavourites = JSON.parse(localStorage.getItem('favorites') || '[]');
    setFavourites(loadedFavourites);
  }, []);

  const handleDeleteClick = async (id: string) => {
    const updatedFavourites = favourites.filter(movie => movie.imdbID !== id);
    setFavourites(updatedFavourites);
    localStorage.setItem('favorites', JSON.stringify(updatedFavourites));
    await playSound();
  };

  const playSound = async () => {
    const audioContext = new AudioContext();
    const response = await fetch(soundFile);
    const arrayBuffer = await response.arrayBuffer();
    const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);
    const source = audioContext.createBufferSource();
    source.buffer = audioBuffer;
    const gainNode = audioContext.createGain();
    gainNode.gain.value = 0.08;
    source.connect(gainNode);
    gainNode.connect(audioContext.destination);
    source.start();
  };

  return (
    <div className="container px-4 mx-auto">
    <div className="flex items-center mb-8">
      <Link to="/" className="flex">
      <img src={cameraIcon} alt="Camera Icon" className="mt-5 mr-4" style={{ width: '70px', height: '70px' }} />
      <h1 className="mt-8 mb-6 text-4xl font-bold">MovieApp</h1>
      </Link>
      </div>
      <h1 className="mt-12 mb-4 text-2xl font-bold">Favourites</h1>
      <div className="grid grid-cols-7 gap-2 p-4 border-b rounded">
        <h2 className="col-span-4 mb-2 text-xl font-semibold">Title</h2>
        <p><strong className="ml-1">Year</strong></p>
        <p><strong className="ml-1">Type</strong></p>
        <p className="text-right"><strong>Actions</strong></p>
      </div>
      {favourites.map((movie) => (
        <div key={movie.imdbID} className="grid grid-cols-7 gap-2 p-1 pt-4 border-b rounded items-center">
            <h2 className="col-span-4 mb-2 text-base sm:text-xl">{movie.Title}</h2>
            <p className="text-sm sm:text-base">{movie.Year}</p>
            <p className="text-sm sm:text-base">{movie.Type}</p>
            <div className="flex flex-col sm:flex-row justify-center sm:justify-end items-center flex-grow">
        <button 
            className="p-2 ml-0 sm:ml-1 flex justify-center items-center" 
            onClick={() => handleDeleteClick(movie.imdbID)}
            onMouseEnter={() => setIsHovered(movie.imdbID)}
            onMouseLeave={() => setIsHovered(null)}
        >
            <img 
            className='min-w-8 min-h-8 transition-transform duration-500 hover:scale-125' 
            src={isHovered === movie.imdbID ? delete_icon : delete_icon_static} 
            alt="Delete" 
            />
        </button>
        <Link 
            to={`/movie/${movie.imdbID}`} 
            className="sm:p-2 p-1 mr-0 sm:mr-1 text-white rounded text-center sm:text-base text-sm flex justify-center items-center"
            style={{
            backgroundImage: 'linear-gradient(45deg, mediumslateblue -30%, darkorange 120%)'
            }}
        >
            Show More
        </Link>
        </div>
        </div>
        ))}
     <footer className="py-5 pr-24 mt-10 mx-[-20px] px-5 text-left border-t-2 border-gray-400 md:pr-80">
        <h1 className="pb-3 font-bold ">MovieApp / by Lukas Scheider</h1>
        <p className="pb-3 text-gray-500">Created in 2024 / April</p>
        <p className="text-gray-500">
            Created with: The ONDb API is a Restful web service to obtain movie information, all content and images on the site are contributed and maintained by our users. More Infos on the API you can find here - 
            <a href="https://www.omdbapi.com/" className="text-blue-500 underline">https://www.omdbapi.com/</a>
        </p>
    </footer>
    </div>
  );
};

export default FavouritesPage;