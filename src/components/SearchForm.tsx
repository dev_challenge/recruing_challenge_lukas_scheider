import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Link } from 'react-router-dom';
import * as yup from "yup";
import { searchMovies } from "../hooks/api";
import cameraIcon from '../assets/video-camera.png';
import '../App.css';
import favoriteIcon from '../assets/favorite_icon.gif';
import favoriteIconStatic from '../assets/favorite_icon_static.gif';
import favoriteIconClicked from '../assets/favorite_icon_clicked.gif';
import soundFile from '../assets/sound.mp3';

const schema = yup.object().shape({
  searchTerm: yup.string().required("Search term is required"),
  type: yup.string().required("Type is required"),
});

type FormData = {
  searchTerm: string;
  type: string;
};

type Results = {
  Search: Array<{
    Title: string;
    Year: string;
    imdbID: string;
    Type: string;
    Poster: string;
    Genre: string;
  }>;
};

interface Movie {
  Title: string;
  Year: string;
  Genre: string;
  Plot: string;
  Type: string;
  Released: string;
  Poster: string;
  imdbID: string;
}

const SearchForm = () => {
  let timer: ReturnType<typeof setTimeout>;
  const [favoriteStatus, setFavoriteStatus] = useState<Record<string, boolean>>({});
  const [isFavoriting, setIsFavoriting] = useState<string | null>(null);
  const [isAnimating, setIsAnimating] = useState<string | null>(null);
  const { register, handleSubmit, formState: { errors }, setValue } = useForm<FormData>({
    resolver: yupResolver(schema),
  });
  const [results, setResults] = useState<Results | null>(null);
  const [modal, setModal] = useState({ visible: false, imageUrl: '', x: 0, y: 0 });

  useEffect(() => {
    const favorites = JSON.parse(localStorage.getItem('favorites') || '[]');
    const favoriteStatus = favorites.reduce((status: Record<string, boolean>, movie: Movie) => {
      status[movie.imdbID] = true;
      return status;
    }, {});
    setFavoriteStatus(favoriteStatus);
  }, []);

  const handleMouseOver = (e: React.MouseEvent, imageUrl: string) => {
    console.log('Mouse over', imageUrl);
    timer = setTimeout(() => {
      const { clientX, clientY } = e;
      setModal({ visible: true, imageUrl, x: clientX + 10, y: clientY + 10 });
    }, 500);
  };

  const playSound = async () => {
    const audioContext = new AudioContext();
    const response = await fetch(soundFile);
    const arrayBuffer = await response.arrayBuffer();
    const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);
    const source = audioContext.createBufferSource();
    source.buffer = audioBuffer;
    const gainNode = audioContext.createGain();
    gainNode.gain.value = 0.08;
    source.connect(gainNode);
    gainNode.connect(audioContext.destination);
    source.start();
  };

  const handleFavoriteClick = async (movie: Movie) => {
    if (isFavoriting) {
      return;
    }
    setIsFavoriting(movie.imdbID);
  
    if (favoriteStatus[movie.imdbID]) {
      const favorites = JSON.parse(localStorage.getItem('favorites') || '[]');
      const index = favorites.findIndex((favorite: Movie) => favorite.imdbID === movie.imdbID);
      if (index !== -1) {
        favorites.splice(index, 1);
        localStorage.setItem('favorites', JSON.stringify(favorites));
        setFavoriteStatus(prevStatus => ({ ...prevStatus, [movie.imdbID]: false }));
      }
    } else {
      const favorites = JSON.parse(localStorage.getItem('favorites') || '[]');
      favorites.push(movie);
      localStorage.setItem('favorites', JSON.stringify(favorites));
      setFavoriteStatus(prevStatus => ({ ...prevStatus, [movie.imdbID]: true }));
      setIsAnimating(movie.imdbID);
      setTimeout(() => setIsAnimating(null), 600);
      await playSound();
    }
  
    setIsFavoriting(null);
  };
  
  useEffect(() => {
    const storedSearchTerm = localStorage.getItem('searchTerm');
    const storedType = localStorage.getItem('type');
    if (storedSearchTerm && storedType) {
      setValue('searchTerm', storedSearchTerm);
      setValue('type', storedType);
      searchMovies(storedSearchTerm, storedType).then((results: Results) => {
        setResults(results);
      });
    }
  }, [setValue]);

  const onSubmit = (data: FormData) => {
    localStorage.setItem('searchTerm', data.searchTerm);
    localStorage.setItem('type', data.type);
    searchMovies(data.searchTerm, data.type).then((results: Results) => {
      setResults(results);
    });
  };

  return (
    <div className="container px-4 mx-auto">
      <div className="flex items-center mb-8">
        <Link to="/" className="flex">
          <img src={cameraIcon} alt="Camera Icon" className="mt-5 mr-4" style={{ width: '70px', height: '70px' }} />
          <h1 className="mt-8 mb-6 text-4xl font-bold">MovieApp</h1>
        </Link>
      </div>
      <form onSubmit={handleSubmit(onSubmit)} className="mb-4">
        <div className="flex items-start w-1/2 pb-3 space-x-4">
          <div className="flex-grow mr-4">
            <h2>Name of movie/series/episode <span className="text-red-500">*</span></h2>
            <input {...register("searchTerm")} placeholder="Search for a movie" className="w-full p-1 mr-2 border rounded outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent" />
            {errors.searchTerm && <p style={{ color: 'red' }}>{errors.searchTerm.message}</p>}
          </div>
          <div>
            <h2 className="mb-1 ">Select type <span className="text-red-500">*</span></h2>
            <div className="flex-col gap-x-4 sm:flex-row">
              <label className="radio-button mr-4">
                Movie
                <input type="radio" value="movie" {...register("type")} />
                <span className="checkmark"></span>
              </label>
              <label className="radio-button">
                Series
                <input type="radio" value="series" {...register("type")} />
                <span className="checkmark"></span>
              </label>
            </div>
            {errors.type && <p style={{ color: 'red' }}>{errors.type.message}</p>}
          </div>
        </div>
        <button type="submit" className="p-2 px-5 font-semibold text-white bg-blue-500 rounded">Search</button>
        <Link to="/favourites">
          <button className="p-2 px-5 ml-5 font-semibold text-white bg-black rounded">Go to Favourites</button>
        </Link>
      </form>
      <h1 className="mt-12 mb-4 text-2xl font-bold">Results</h1>
      <div className="grid grid-cols-7 gap-2 p-4 border-b rounded">
        <h2 className="col-span-3 mb-2 text-base sm:text-xl font-semibold">Title</h2>
        <p><strong className="ml-1 text-sm sm:text-base">Year</strong></p>
        <p><strong className="ml-1 text-sm sm:text-base">Type</strong></p>
        <p className="col-span-2 text-right text-sm sm:text-base"><strong>Detail View</strong></p>
      </div>
      {results && results.Search && results.Search.length > 0 ? (
      results.Search.map((movie) => (
          <div 
            key={movie.imdbID} 
            className="grid grid-cols-7 gap-2 p-1 pt-4 border-b rounded items-center movie-card"
            onMouseOver={(e) => handleMouseOver(e, movie.Poster)}
            onMouseOut={() => {
              clearTimeout(timer);
              setModal({ visible: false, imageUrl: '', x: 0, y: 0 });
            }}
          >
          <h2 className="col-span-3 mb-2 text-base sm:text-xl flex items-center">{movie.Title}</h2>
          <p className="text-sm sm:text-base flex items-center">{movie.Year}</p>
          <p className="text-sm sm:text-base flex items-center">{movie.Genre}</p>
          <div className="col-start-7 flex flex-col items-center justify-center sm:justify-end sm:flex-row">
      <button className='pointer-events-none min-h-8 min-w-8 w-8 h-8 mr-2' onClick={() => handleFavoriteClick(movie as Movie)}>
        <img className='pointer-events-auto' src={isAnimating === movie.imdbID ? favoriteIcon : (isFavoriting === movie.imdbID ? favoriteIcon : (favoriteStatus[movie.imdbID] ? favoriteIconClicked : favoriteIconStatic))} alt="Favorite" />
      </button>
      <Link 
      to={`/movie/${movie.imdbID}`} 
      className="flex justify-center p-2 mr-2 text-white rounded text-sm sm:text-base text-center items-center glowing-button"
      style={{
        backgroundImage: 'linear-gradient(45deg, mediumslateblue -30%, darkorange 120%)'
      }}
    >
      Show More
    </Link>
    </div>
        </div>
      ))
    ) : (
      <p className="flex w-full text-xl mt-2 justify-center items-center text-red-700">No data available. Try another search term.</p>
    )}
    {modal.visible && (
      <div className="modal" style={{ left: `${modal.x}px`, top: `${modal.y}px` }}>
        <img src={modal.imageUrl} alt="Movie" />
      </div>
    )}
      <footer className="py-5 pr-24 mt-10 mx-[-20px] px-5 text-left border-t-2 border-gray-400 md:pr-80">
        <h1 className="pb-3 font-bold ">MovieApp / by Lukas Scheider</h1>
        <p className="pb-3 text-gray-500">Created in 2024 / April</p>
        <p className="text-gray-500">
          Created with: The ONDb API is a Restful web service to obtain movie information, all content and images on the site are contributed and maintained by our users. More Infos on the API you can find here - 
          <a href="https://www.omdbapi.com/" className="text-blue-500 underline">https://www.omdbapi.com/</a>
        </p>
      </footer>
    </div>
  );
};

export default SearchForm;


