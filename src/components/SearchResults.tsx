import { useLocation } from "react-router-dom";

type Movie = {
  imdbID: string;
  Title: string;
  Poster: string;
};

const SearchResults = () => {
  const location = useLocation();
  const results = location.state.results;

  if (!results || !results.Search) {
    return <div>No movies or series for your search term</div>;
  }

  return (
    <div>
      {results.Search.map((movie: Movie) => (
        <div key={movie.imdbID}>
          <h2>{movie.Title}</h2>
          <img src={movie.Poster} alt={movie.Title} />
        </div>
      ))}
    </div>
  );
};

export default SearchResults;