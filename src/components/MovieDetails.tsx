import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { getMovieDetails } from '../hooks/api';
import { AxiosError } from 'axios';
import cameraIcon from '../assets/video-camera.png';
import { Link } from 'react-router-dom';
import favoriteIcon from '../assets/favorite_icon.gif';
import favoriteIconStatic from '../assets/favorite_icon_static.gif';
import favoriteIconClicked from '../assets/favorite_icon_clicked.gif';
import soundFile from '../assets/sound.mp3';

interface Movie {
  Title: string;
  Year: string;
  Genre: string;
  Plot: string;
  Type: string;
  Released: string;
  Poster: string;
  imdbID: string;
}

const MovieDetails = () => {
  const { id } = useParams();
  const [movie, setMovie] = useState<Movie | null>(null);
  const [favoriteStatus, setFavoriteStatus] = useState<Record<string, boolean>>({});
  const [isFavoriting, setIsFavoriting] = useState<string | null>(null);
  const [isAnimating, setIsAnimating] = useState<string | null>(null);

  const playSound = async () => {
    const audioContext = new AudioContext();
    const response = await fetch(soundFile);
    const arrayBuffer = await response.arrayBuffer();
    const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);
    const source = audioContext.createBufferSource();
    source.buffer = audioBuffer;
    const gainNode = audioContext.createGain();
    gainNode.gain.value = 0.08;
    source.connect(gainNode);
    gainNode.connect(audioContext.destination);
    source.start();
  };

  useEffect(() => {
    const favorites = JSON.parse(localStorage.getItem('favorites') || '[]');
    const favoriteStatus: Record<string, boolean> = {};
    favorites.forEach((favorite: Movie) => {
      favoriteStatus[favorite.imdbID] = true;
    });
    setFavoriteStatus(favoriteStatus);
  }, []);

  useEffect(() => {
    if (id) {
      getMovieDetails(id).then((movie: Movie) => {
        setMovie(movie);
      }).catch((error: AxiosError) => {
        console.error("Failed to fetch movie details", error);
      });
    }
  }, [id]);

  const handleFavoriteClick = async (movie: Movie) => {
    if (isFavoriting) {
      return;
    }
    setIsFavoriting(movie.imdbID);
  
    if (favoriteStatus[movie.imdbID]) {
      const favorites = JSON.parse(localStorage.getItem('favorites') || '[]');
      const index = favorites.findIndex((favorite: Movie) => favorite.imdbID === movie.imdbID);
      if (index !== -1) {
        favorites.splice(index, 1);
        localStorage.setItem('favorites', JSON.stringify(favorites));
        setFavoriteStatus(prevStatus => ({ ...prevStatus, [movie.imdbID]: false }));
      }
    } else {
      const favorites = JSON.parse(localStorage.getItem('favorites') || '[]');
      favorites.push(movie);
      localStorage.setItem('favorites', JSON.stringify(favorites));
      setFavoriteStatus(prevStatus => ({ ...prevStatus, [movie.imdbID]: true }));
      setIsAnimating(movie.imdbID);
      setTimeout(() => setIsAnimating(null), 600);
      await playSound();
    }
  
    setIsFavoriting(null);
  };

  
  if (!movie) return null;

  return (
    <div className="container flex flex-col min-h-screen px-4 mx-auto">
    <div className="flex items-center mb-8">
    <Link to="/" className="flex">
      <img src={cameraIcon} alt="Camera Icon" className="mt-5 mr-4" style={{ width: '70px', height: '70px' }} />
      <h1 className="mt-8 mb-6 text-4xl font-bold">MovieApp</h1>
    </Link>
    </div>
      <div className="flex flex-col sm:flex-row">
        <img src={movie.Poster} alt={movie.Title} className="object-cover w-64 my-4 h-96" />
        <div className="flex flex-col justify-center mb-12   sm:mb-48 sm:ml-32">
        <h2 className="mb-4 text-4xl font-bold">{movie.Title}</h2>
        <p><strong className='text-xl'>Genre:</strong> {movie.Genre}</p>
        <p className="mt-4 mb-2"><strong className='text-xl'>Save to Favourites</strong></p>
        <button className='pointer-events-none' onClick={() => handleFavoriteClick(movie)}>
      <img className='pointer-events-auto' src={isAnimating === movie.imdbID ? favoriteIcon : (isFavoriting === movie.imdbID ? favoriteIcon : (favoriteStatus[movie.imdbID] ? favoriteIconClicked : favoriteIconStatic))} alt="Favorite" />
    </button>
        </div>
      </div>
      <div className='grid grid-cols-2 gap-6' style={{ gridTemplateColumns: 'auto 1fr' }}>
        <strong>Title:</strong> <span>{movie.Title}</span>
        <strong>Year:</strong> <span>{movie.Year}</span>
        <strong>Genre:</strong> <span>{movie.Genre}</span>
        <strong>Plot:</strong> <span>{movie.Plot}</span>
        <strong>Type:</strong> <span>{movie.Type}</span>
        <strong>Released:</strong> <span>{movie.Released}</span>
     </div>
     <footer className="py-5 pr-24 mt-10 mx-[-20px] px-5 text-left border-t-2 border-gray-400 md:pr-80">
        <h1 className="pb-3 font-bold ">MovieApp / by Lukas Scheider</h1>
        <p className="pb-3 text-gray-500">Created in 2024 / April</p>
        <p className="text-gray-500">
            Created with: The ONDb API is a Restful web service to obtain movie information, all content and images on the site are contributed and maintained by our users. More Infos on the API you can find here - 
            <a href="https://www.omdbapi.com/" className="text-blue-500 underline">https://www.omdbapi.com/</a>
        </p>
    </footer>
    </div>
  );
};

export default MovieDetails;