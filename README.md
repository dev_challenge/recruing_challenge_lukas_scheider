# Recruiting Dev Challenge Movie App
A Movie search tool to let the user search gifs and mark them as favourites.

## Getting started
Using Terminal in project root directory:
- npm install
- npm run dev

## Usage
Search Movies in the Dashboard.
Save and Remove Movies in the Favourites Page while enjoing the animations and design.

## Building docker image for raspberry pi
- create dockerimage
`docker buildx build --platform linux/arm64 -t movieapp .`
- copy dockerimage to project root directory
`docker save -o movieapp.tar movieapp`
- copy dockerimage to usb drive and insert it into raspberry
- copy dockerimage to raspberry local storage
- import the docker image 
`docker load -i movieapp.tar`
- run the docker image and add it to the same docker-network as the nginx reverseproxy
`docker run --network reverse-proxy-network movieapp`